package ab

import org.scalatest.EitherValues
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import cats.syntax.either._

class PlayedFramesSpec extends AnyFreeSpec with EitherValues {

  "PlayedFrames" - {

    "should calculate current score for sequence of Frames" in {

      val frames = Strike(1) ::
        Spare(2, 7) ::
        OpenFrame(3, 7, Option(2)) ::
        Spare(4, 9) ::
        Strike(5) ::
        Strike(6) ::
        Strike(7) ::
        OpenFrame(8, 2, Option(3)) ::
        Spare(9, 6) ::
        FinalFrame(10, 7, Option(3), Option(3)) ::
        Nil

      val playedFrames = PlayedFrames(frames.reverse)
      PlayedFrames.calculateScore(playedFrames) shouldBe 168
    }

    "should play Frames and calculate score" in {
      val knockdownPins = List(
        10,
        7, 3,
        7, 2,
        9, 1,
        10,
        10,
        10,
        2, 3,
        6, 4,
        7, 3, 3
      )
      val allPlayedFrames = knockdownPins.foldLeft(PlayedFrames.initFrames) { (playedFrames, pins) =>
        PlayedFrames.knockdownPins(playedFrames, pins).right.value
      }

      PlayedFrames.calculateScore(allPlayedFrames) shouldBe 168
    }

    "should play Frames and calculate score for missed shots" in {
      val knockdownPins = List(0, 2, 4, 5, 3, 7, 3, 4, 2, 1, 6, 4, 10, 9, 1, 2, 6, 3, 2)

      val allPlayedFrames = knockdownPins.foldLeft(PlayedFrames.initFrames) { (playedFrames, pins) =>
        PlayedFrames.knockdownPins(playedFrames, pins).right.value
      }

      PlayedFrames.calculateScore(allPlayedFrames) shouldBe 99
    }

    "should play Frames for all missed shots" in {
      val knockdownPins = List.fill(20)(0)

      val allPlayedFrames = knockdownPins.foldLeft(PlayedFrames.initFrames) { (playedFrames, pins) =>
        PlayedFrames.knockdownPins(playedFrames, pins).right.value
      }

      PlayedFrames.calculateScore(allPlayedFrames) shouldBe 0
    }

    "should play Frames with max scores" in {
      val knockdownPins = List.fill(12)(10)

      val allPlayedFrames = knockdownPins.foldLeft(PlayedFrames.initFrames) { (playedFrames, pins) =>
        PlayedFrames.knockdownPins(playedFrames, pins).right.value
      }

      PlayedFrames.calculateScore(allPlayedFrames) shouldBe 300
    }

    "should fail the game for too many shots" in {
      val knockdownPins = List.fill(21)(1)

      val allPlayedFrames = knockdownPins.foldLeft(PlayedFrames.initFrames.asRight[String]) { (playedFrames, pins) =>
        playedFrames match {
          case Right(frames) => PlayedFrames.knockdownPins(frames, pins)
          case failure => failure
        }
      }

      allPlayedFrames.isLeft shouldBe true
    }

    "should play Frames for not enough shots " in {
      val knockdownPins = List.fill(8)(1)

      val allPlayedFrames = knockdownPins.foldLeft(PlayedFrames.initFrames) { (playedFrames, pins) =>
        PlayedFrames.knockdownPins(playedFrames, pins).right.value
      }

      PlayedFrames.calculateScore(allPlayedFrames) shouldBe 8
    }

  }
}
