package ab

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class FramesSpec extends AnyFreeSpec with Matchers {

  "FinalFrame" - {

    "should apply 2nd shot" in {
      Frame.mkFrame(FinalFrame(10, 6, None, None), 3) shouldBe Right(FinalFrame(10, 6, Option(3), Some(0)))
    }

    "should fail for invalid 2nd shot" in {
      Frame.mkFrame(FinalFrame(10, 6, None, None), 5) shouldBe Left("Invalid pins: 5 for FinalFrame(10,6,None,None)")
    }

    "should apply 3rd shot" in {
      Frame.mkFrame(FinalFrame(10, 6, Option(4), None), 5) shouldBe Right(FinalFrame(10, 6, Option(4), Option(5)))
    }

    "should fail for invalid 3rd shot" in {
      Frame.mkFrame(FinalFrame(10, 10, Some(3), None), 8) shouldBe Left("Invalid pins: 8 for FinalFrame(10,10,Some(3),None)")
    }

  }

  "OpenFrame" - {

    "should apply 2nd shot" in {
      Frame.mkFrame(OpenFrame(1, 6, None), 3) shouldBe Right(OpenFrame(1, 6, Option(3)))
    }

    "should make Spare" in {
      Frame.mkFrame(OpenFrame(1, 6, None), 4) shouldBe Right(Spare(1, 6))
    }

    "should make Strike" in {
      Frame.mkFrame(OpenFrame(1, 6, Option(1)), 10) shouldBe Right(Strike(2))
    }

    "should return failure if the total score is more than 10" in {
      Frame.mkFrame(OpenFrame(1, 6, None), 5) shouldBe Left("Invalid pins: 5 for OpenFrame(1,6,None)")
    }

    "should make Final Frame" in {
      Frame.mkFrame(OpenFrame(9, 6, Option(1)), 2) shouldBe Right(FinalFrame(10, 2, None, None))
    }
  }

  "Strike" - {
    "should apply next shot" in {
      Frame.mkFrame(Strike(2), 4) shouldBe Right(OpenFrame(3, 4, None))
    }

    "should make final frame" in {
      Frame.mkFrame(Strike(9), 4) shouldBe Right(FinalFrame(10, 4, None, None))
    }
  }

  "Spare" - {
    "should apply next shot" in {
      Frame.mkFrame(Spare(2, 3), 4) shouldBe Right(OpenFrame(3, 4, None))
    }
    "should make final frame" in {
      Frame.mkFrame(Spare(9, 3), 4) shouldBe Right(FinalFrame(10, 4, None, None))
    }
  }
}
