package ab

trait PinsValidating {

  def validateKnockedDownPins(pins: Int): ResultOr[Int] =
    Either.cond[String, Int](pins >= 0 && pins <= 10, pins, s"Knocked down pins has to be between 1 and 10, while it was: $pins")

}
