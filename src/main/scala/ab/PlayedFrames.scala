package ab

/**
 * Holds pin knockdowns (`Frame`s) for a given player.
 */
case class PlayedFrames private(frames: List[Frame]) {
  def currentFrame: Option[Int] = frames.headOption.map(_.id)
}

/**
 * Provides API for `PlayedFrames`.
 */
object PlayedFrames extends PinsValidating {

  val MAX_PINS = 10

  private case class ScoreAccumulator(total: Int, oneRollAgo: Int, twoRollsAgo: Int)

  def initFrames: PlayedFrames = PlayedFrames(List.empty[Frame])

  /**
   * Updates / creates new `Frame` for given number of knocked down pins.
   */
  def knockdownPins(playedFrames: PlayedFrames, pins: Int): ResultOr[PlayedFrames] =
    for {
      knockedDownPins <- validateKnockedDownPins(pins)
      frames <- playFrame(playedFrames, knockedDownPins)
    } yield {
      frames
    }

  private def playFrame(playedFrames: PlayedFrames, pins: Int): ResultOr[PlayedFrames] = {
    playedFrames match {
      case PlayedFrames(Nil) =>
        Frame(pins).map(f => PlayedFrames(List(f)))
      case PlayedFrames(h :: t) =>
        Frame.mkFrame(h, pins).map { f =>
          if (f.id == h.id) PlayedFrames(f :: t)
          else PlayedFrames(f :: h :: t)
        }
    }
  }

  private implicit def optScoreToScore(score: Option[Int]): Int = score.getOrElse(0)

  /**
   * Calculate scores from all played Frames assuming some defaults.
   * It is not this function responsibility to validate Frames being correctly built - it simply sums all scores up.
   *
   * NOTE: if we wanted this function to validate Frames then result should be ResultOr rather than raw value.
   */
  def calculateScore(playedFrames: PlayedFrames): Int = {

    playedFrames.frames.foldLeft(ScoreAccumulator(0, 0, 0)) { (acc, frame) =>
      frame match {
        case FinalFrame(_, firstShot, secondShot, thirdShot) =>
          ScoreAccumulator(firstShot + secondShot + thirdShot, firstShot, secondShot)
        case OpenFrame(_, firstShot, secondShot) =>
          ScoreAccumulator(acc.total + firstShot + secondShot, firstShot, secondShot)
        case Spare(_, score) =>
          ScoreAccumulator(acc.total + MAX_PINS + acc.oneRollAgo, score, MAX_PINS - score)
        case Strike(_) =>
          ScoreAccumulator(acc.total + MAX_PINS + acc.oneRollAgo + acc.twoRollsAgo, MAX_PINS, acc.oneRollAgo)
      }
    }.total
  }
}

