package ab

import cats.syntax.either._

import scala.annotation.tailrec

object BowlingApp extends App {

  println("Game started")

  val result = getInputShots()
    .flatMap(play(PlayedFrames.initFrames))

  println(s"Game results: $result")

  @tailrec
  private def play(frames: PlayedFrames)(pins: List[Int]): ResultOr[Int] = pins match {
    case Nil => PlayedFrames.calculateScore(frames).asRight[String]
    case shot :: remaining =>
      PlayedFrames.knockdownPins(frames, shot) match {
        case Right(f) => play(f)(remaining)
        case Left(errMsg) => errMsg.asLeft[Int]
      }
  }

  private def getInputShots(): ResultOr[List[Int]] = {
    Either
      .catchNonFatal(args.map(_.toInt).toList)
      .leftMap(_.getMessage)
  }
}

