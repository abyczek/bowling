package object ab {

  /**
   * Type alias holding result of the computation.
   */
  type ResultOr[T] = Either[String, T]
}
