package ab

import cats.syntax.either._

/**
 * `Frame` represents state of given bowling frame.
 * Individual states (e.g. Strike, Spare etc) are represented by individual case classes.
 */
sealed trait Frame {

  /**
   * Frame unique id (range between 1 and 10)
   */
  val id: Int
}

object Frame extends PinsValidating {

  val MAX_PINS = 10

  def apply(pins: Int): ResultOr[Frame] = pins match {
    case MAX_PINS => Strike(1).asRight[String]
    case p if p >= 0 && p < 10 => OpenFrame(1, pins, None).asRight[String]
    case _ => s"Cannot create Frame for pins value: $pins".asLeft[Frame]
  }

  def mkFrame(frame: Frame, pins: Int): ResultOr[Frame] = {
    for {
      pins <- validateKnockedDownPins(pins)
      _ <- validateCurrentFrame(frame)
      frame <- applyPins(frame, pins)
    } yield {
      frame
    }
  }

  private def applyPins(frame: Frame, pins: Int): ResultOr[Frame] =
    frame match {
      case f@FinalFrame(_, 10, None, None) => f.copy(secondShot = Option(pins)).asRight
      case f@FinalFrame(_, 10, Some(10), None) => f.copy(thirdShot = Option(pins)).asRight
      case f@FinalFrame(_, 10, Some(second), None) if second + pins <= 10 => f.copy(thirdShot = Option(pins)).asRight
      case f@FinalFrame(_, first, None, None) if first + pins < 10 => f.copy(secondShot = Option(pins), thirdShot = Option(0)).asRight
      case f@FinalFrame(_, first, None, None) if first + pins == 10 => f.copy(secondShot = Option(pins)).asRight
      case f@FinalFrame(_, _, Some(second), None) if second + pins <= 10 => f.copy(thirdShot = Option(pins)).asRight
      case f: FinalFrame => Left(s"Invalid pins: $pins for $f")

      case OpenFrame(_, firstShot, None) if firstShot + pins == MAX_PINS => Spare(frame.id, firstShot).asRight
      case f@OpenFrame(_, firstShot, None) => Either
        .cond[String, Frame](firstShot + pins < MAX_PINS, f.copy(secondShot = Option(pins)), s"Invalid pins: $pins for $f")

      case f: Frame if f.id == 9 => FinalFrame(10, pins, None, None).asRight

      case f: Frame if pins == 10 => Strike(f.id + 1).asRight

      case f: Frame => OpenFrame(f.id + 1, pins, None).asRight
    }

  private def validateCurrentFrame(frame: Frame): ResultOr[Unit] =
    Either.cond[String, Unit](frame.id > 0 && frame.id <= 10, (), "Frame id has to be between 1 and 10 while it was: $")
}

final case class Strike(id: Int) extends Frame

final case class Spare(id: Int, firstShot: Int) extends Frame

final case class OpenFrame(id: Int, firstShot: Int, secondShot: Option[Int]) extends Frame

final case class FinalFrame(id: Int, firstShot: Int, secondShot: Option[Int], thirdShot: Option[Int]) extends Frame
